(function(){
    var app = angular.module("contactInfo", []);
    app.directive('contactTable', ['$http', function($http){
        return {
            restrict: 'E',
            templateUrl: './app/directives/contact-table.html',
            controller: function($scope, $http){
                $scope.model = {
                    contacts: [],
                    selected: {}
                };

                $http.get('http://localhost:3000/contacts').success(function(data){
                    $scope.model.contacts = data;
                });
            
                // gets the template to ng-include for a table row / item
                $scope.getTemplate = function (contact) {
                    if (contact.id === $scope.model.selected.id) return 'edit';
                    else return 'display';
                };
            
                $scope.editContact = function (contact) {
                    $scope.model.selected = angular.copy(contact);
                };
            
                $scope.saveContact = function (idx) {
                    console.log("Saving contact");
                    $scope.model.contacts[idx] = angular.copy($scope.model.selected);
                    $scope.reset();
                };
            
                $scope.reset = function () {
                    $scope.model.selected = {};
                };
            },
            controllerAs: 'ContactTableController'
        };
    }]);
})();